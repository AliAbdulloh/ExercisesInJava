import java.util.HashMap;
import java.util.Map;

public class Leetcode1859 {
    public String sortSentence(String s) {
        Map<Integer, String> map = new HashMap<>();

        String[] words = s.split(" ");

        for (String word : words) {
            map.put(getNumber(word), getWord(word));
        }
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < map.size() - 1; i++) {
            builder.append(map.get(i + 1)).append(' ');
        }

        if (map.size() > 0) {
            builder.append(map.get(map.size()));
        }

        return builder.toString();
    }

    public int getNumber(String word) {
        int k = 1;
        int index = word.length() - 1;
        int number = 0;

        while (Character.isDigit(word.charAt(index))) {
            number += (word.charAt(index) - '0') * k;
            k *= 10;
            index--;
        }
        return number;
    }

    public String getWord(String word) {
        int index = word.length() - 1;

        while (Character.isDigit(word.charAt(index))) {
            index--;
        }
        return word.substring(0, index + 1);
    }
}
