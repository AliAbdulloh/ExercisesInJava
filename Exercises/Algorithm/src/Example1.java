/*class Animal {
    void m1(String x){
        System.out.println(x);
    }
}

class Dog extends Animal {

}

public class Example1{
    public static void main(String[] args){
        Animal a = new Dog();
        a.m1("Salom jigar");
    }
}*/
class Animal {
    void m1(String x){
        System.out.println("One");
    }
}

class Cat extends Animal {
    public void m1(String x){
        System.out.println("Two");
        super.m1(null);
    }
}

public class Example1{
    public static void main(String[] args){
        Animal a = new Cat();
        a.m1(null);
    }
}
