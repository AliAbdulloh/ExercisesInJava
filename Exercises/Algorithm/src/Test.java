import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

class Al{
    void m1() {
        System.out.println("m1 in class Al");
    }
}

 class B extends Al{
    void m1() {
        super.m1();
        System.out.println("m1 in class B");
    }
}
class C extends B{
    void m1() {
        System.out.println("m1 in class C");
    }
}

public class Test {
    public static void main(String[] args){

        C c = new C();
        c.m1();
        Static.a();
    }
}