package com.company.mapstruct.person;

import org.junit.jupiter.api.Test;

import static com.company.mapstruct.person.PersonMapper.PERSON_MAPPER;
import static org.junit.jupiter.api.Assertions.*;

class PersonMapperTest {

    @Test
    void toEntity() {
        PersonDTO personDTO = new PersonDTO("Abdulloh",27);
        AddressDTO addressDTO = new AddressDTO("Toshkent","Bog'ko'cha 17, 41");
        PassportDTO passportDTO = new PassportDTO("AA","1234567");
        Person person = PERSON_MAPPER.toEntity(personDTO, addressDTO, passportDTO);
        System.out.println("person = " + person);
    }
}