package com.company.mapstruct;

import com.company.mapstruct.car.Car;
import com.company.mapstruct.car.CarDTO;
import com.company.mapstruct.car.CarMapper;
import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;
import org.springframework.boot.test.context.SpringBootTest;

import static com.company.mapstruct.car.CarMapper.CAR_MAPPER;

@SpringBootTest
class MapstructApplicationTests {
	@Test
	void contextLoads() {
		Car car = new Car("1", "Cobalt", "GM",1200);
		CarDTO dto = CAR_MAPPER.toDTO(car);
		System.out.println("dto = " + dto);
	}
}
