package com.company.mapstruct.project;

import com.company.mapstruct.projectColumn.ProjectColumn;
import org.junit.jupiter.api.Test;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;

import static com.company.mapstruct.project.ProjectMapper.PROJECT_MAPPER;
import static org.junit.jupiter.api.Assertions.*;

class ProjectMapperTest {

    @Test
    void toDTO() {
        List<ProjectColumn> projectColumns = List.of(
                new ProjectColumn("1","Todo",1,LocalDateTime.now()),
                new ProjectColumn("2","Done",2,LocalDateTime.now()),
                new ProjectColumn("3","QA",3,LocalDateTime.now()),
                new ProjectColumn("4","Doing",4,LocalDateTime.now()),
                new ProjectColumn("5","Perfect",5,LocalDateTime.now())
        );
        Project project = new Project("1", "New Backend Project", Path.of("https://online.pdp.uz"),
                projectColumns,
                LocalDateTime.now());
        ProjectDTO projectDTO = PROJECT_MAPPER.toDTO(project);
        System.out.println("projectDTO = " + projectDTO);
    }
}