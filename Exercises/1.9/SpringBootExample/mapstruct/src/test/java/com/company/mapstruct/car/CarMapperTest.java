package com.company.mapstruct.car;

import org.junit.jupiter.api.Test;
import org.mapstruct.factory.Mappers;

import static org.junit.jupiter.api.Assertions.*;

class CarMapperTest {

    @Test
    void toDTO() {
        Car car = new Car("1", "Cobalt", "GM", 1200);
        CarMapper carMapper = Mappers.getMapper(CarMapper.class);
        CarDTO dto = carMapper.toDTO(car);
        System.out.println("dto = " + dto);
        Car entity = carMapper.toEntity(dto);
        System.out.println("entity = " + entity);
    }
}