package com.company.mapstruct;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SimpleController {
    @Value("${example.string}")
    private String exampleString;

    @GetMapping("/exampleString")
    public String exampleString(){
        return exampleString;
    }
}