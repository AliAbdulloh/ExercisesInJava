package com.company.mapstruct.projectColumn;

import org.mapstruct.Mapping;

public interface ProjectColumnMapper {
    @Mapping(target = "pc_name", source = "name")
    @Mapping(target = "pc_order", source = "order")
    @Mapping(target = "pc_createdAt", source = "createdAt",dateFormat = "dd.MM.YYYY")
    ProjectColumnDTO toDTO(ProjectColumn projectColumn);
}
