package com.company.mapstruct.project;

import com.company.mapstruct.projectColumn.ProjectColumn;
import com.company.mapstruct.projectColumn.ProjectColumnDTO;
import lombok.*;
import org.springframework.cglib.core.Local;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectDTO {
    private String id;
    private String name;
    private String documentPath;
    private List<ProjectColumnDTO> projectColumnDTO;
    private String createdAt;
}
