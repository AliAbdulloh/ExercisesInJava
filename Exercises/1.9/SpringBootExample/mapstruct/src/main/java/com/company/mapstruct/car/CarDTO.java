package com.company.mapstruct.car;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarDTO {
    private String carName;
    private double carPrice;
}
