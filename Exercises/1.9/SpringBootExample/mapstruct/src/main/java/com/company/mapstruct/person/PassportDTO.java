package com.company.mapstruct.person;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PassportDTO {
    private String serial;
    private String number;
}
