package com.company.mapstruct.car;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
public class Car {
    private String id;
    private String name;
    private String maker;
    private double price;
}
