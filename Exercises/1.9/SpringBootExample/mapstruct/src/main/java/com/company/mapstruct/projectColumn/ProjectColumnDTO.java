package com.company.mapstruct.projectColumn;

import lombok.*;

import java.time.LocalDateTime;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProjectColumnDTO {
    private String pc_name;
    private String pc_order;
    private String pc_createdAt;
}
