package com.company.mapstruct.person;

import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Person {
    private String fullName;
    private Integer age;

    private String personAddressCity;
    private String personAddressApartment;

    private String personPasswordSerial;
    private String personPasswordNumber;
}
