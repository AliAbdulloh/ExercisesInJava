package com.company.mapstruct.project;

import com.company.mapstruct.projectColumn.ProjectColumn;
import lombok.*;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.List;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Project {
    private String id;
    private String name;
    private Path documentPath;
    private List<ProjectColumn> projectColumns;
    private LocalDateTime createdAt;
}
