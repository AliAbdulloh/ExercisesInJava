package com.company.springboot.dto;

public record TokenRequest(String username, String password) {
}
