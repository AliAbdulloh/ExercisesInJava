package com.company.springboot.dto;

import lombok.Getter;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.TimeZone;

@Getter
public class AppErrorDTO {
    private String errorMessage;
    private String errorPath;
    private Integer errorCode;
    private LocalDateTime timestamp;

    public AppErrorDTO(String errorMessage, String errorPath, Integer errorCode) {
        this.errorMessage = errorMessage;
        this.errorPath = errorPath;
        this.errorCode = errorCode;
        this.timestamp = LocalDateTime.now(Clock.system(ZoneId.of("Asia/Tashkent")));
    }
}
