package com.company.springdoc.comment;

import com.company.springdoc.EntityNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/comments")
@Tag(name = "Comment Controller", description = "Ushbu controller comment  bilan ishlash uchun yaratilgan")
@ParameterObject
public class CommentController {
    private final CommentRepository commentRepository;

    public CommentController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }


    @GetMapping("/{id}")
    @Operation(summary = "Id bo'yicha comment olib kelish")
    public ResponseEntity<Comment> getComment(@PathVariable Integer id) {
        Comment comment = commentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Comment not found with ID: " + id));
        return ResponseEntity.ok(comment);
    }

    @GetMapping("/")
    public ResponseEntity<List<Comment>> getAllComment(){
        List<Comment> comments = commentRepository.findAll();
        return ResponseEntity.ok(comments);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteComment(@PathVariable Integer id){
        Comment comment = commentRepository.findById(id)
                .orElseThrow(()-> new EntityNotFoundException("Comment not found with ID: " + id));
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/")
    public ResponseEntity<Comment> saveComment(@RequestBody Comment comment){
        return ResponseEntity.status(HttpStatus.CREATED).body(commentRepository.save(comment));
    }
}
