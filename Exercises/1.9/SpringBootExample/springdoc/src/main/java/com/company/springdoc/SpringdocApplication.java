package com.company.springdoc;

import com.company.springdoc.comment.Comment;
import com.company.springdoc.comment.CommentRepository;
import com.company.springdoc.post.Post;
import com.company.springdoc.post.PostRepository;
import com.company.springdoc.todo.Todo;
import com.company.springdoc.todo.TodoRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.ExternalDocumentation;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.info.License;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import io.swagger.v3.oas.models.servers.Server;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.net.URL;
import java.util.List;

@SpringBootApplication
/*@OpenAPIDefinition(
        info = @Info(
                title = "Test Application Java(Springdoc)",
                version = "1",
                contact = @Contact(
                        name = "Azimov Elyor",
                        email = "aliabdulloh01@mail.ru",        //email yoki gamil yozilagi
                        url = "https://github.com/aliabdullo"   //github accaunt url
                ),
                license = @License(
                        name = "Apache 2.0",
                        url = "https://springdoc.org"
                ),
                termsOfService = "https://swagger.io/terms",
                description = "This document is a project intended for employee testing"
        ),
        externalDocs = @ExternalDocumentation(
                description = "SpringDOC version 2",
                url = "https://springdoc.org/v/2"
        ),
        servers = {
                @Server(
                        url = "http://localhost:8080",
                        description = "Production Server"
                ),
                @Server(
                        url = "http://localhost:9090",
                        description = "Test Server"
                )}
)
@SecurityScheme(
        name = "basicAuth",
        type = SecuritySchemeType.HTTP,
        scheme = "basic"
)
@SecurityScheme(
        name = "Bearer Authentication",
        type = SecuritySchemeType.HTTP,
        bearerFormat = "JWT",
        scheme = "bearer"
)*/
public class SpringdocApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringdocApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(
            ObjectMapper objectMapper,
            CommentRepository commentRepository,
            PostRepository postRepository,
            TodoRepository todoRepository
    ) {
        return (args -> {
            List<Comment> comments = objectMapper.readValue(new URL("https://jsonplaceholder.typicode.com/comments"), new TypeReference<>() {
            });
            commentRepository.saveAll(comments);
            List<Post> posts = objectMapper.readValue(new URL("https://jsonplaceholder.typicode.com/posts"), new TypeReference<>() {
            });
            postRepository.saveAll(posts);
            List<Todo> todos = objectMapper.readValue(new URL("https://jsonplaceholder.typicode.com/todos"), new TypeReference<>() {
            });
            todoRepository.saveAll(todos);
        });
    }

    @Bean
    public WebMvcConfigurer corsConfigurer() {
        return new WebMvcConfigurer() {
            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowedOrigins("*");
            }
        };
    }

    @Bean
    public OpenAPI springOpenAPI() {
        return new OpenAPI()
                .info(new Info()
                        .title("Test Application Java(Springdoc)")
                        .description("This document is a project intended for employee testing")
                        .version("1")
                        .contact(new Contact()
                                .name("Azimov Elyor")
                                .email("aliabdulloh01@mail.ru")      //email yoki gamil yozilagi
                                .url("https://github.com/aliabdullo")   //github accaunt url))
                        )
                        .license(new License()
                                .name("Apache 2.0")
                                .url("https://springdoc.org"))
                        .termsOfService("https://swagger.io/terms"))
                .externalDocs(new ExternalDocumentation()
                        .description("SpringShop Wiki Documentation")
                        .url("https://springShop.wiki.github.org/docs"))
                .servers(List.of(
                        new Server().url("http://localhost:8080").description("Production Server"),
                        new Server().url("http://localhost:9090").description("Test Server")
                )).addSecurityItem(new SecurityRequirement().addList("basicAuth", "bearerAuth"))
                .components((new Components()
                        .addSecuritySchemes("basicAuth", new SecurityScheme()
                                .name("basicAuth")
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("basic"))
                        .addSecuritySchemes("bearerAuth", new SecurityScheme()
                                .name("bearerAuth")
                                .type(SecurityScheme.Type.HTTP)
                                .scheme("bearer")
                                .bearerFormat("JWT"))
                ));
    }

    @Bean
    public GroupedOpenApi postOpenAPI() {
        return GroupedOpenApi.builder()
                .group("post")
                .pathsToMatch("/api/posts/**")
                .build();
    }

    @Bean
    public GroupedOpenApi commentOpenAPI() {
        return GroupedOpenApi.builder()
                .group("comment")
                .pathsToMatch("/api/comments/**")
                .build();
    }

    @Bean
    public GroupedOpenApi todoOpenAPI() {
        return GroupedOpenApi.builder()
                .group("todo")
                .pathsToMatch("/api/todos/**")
                .build();
    }
}
