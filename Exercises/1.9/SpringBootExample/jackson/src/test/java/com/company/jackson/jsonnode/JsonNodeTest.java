package com.company.jackson.jsonnode;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class JsonNodeTest {
    @Test
    void simpleCodeTest() throws Exception {
        String JsonSTRING = """
                {
                  "id": 1,
                  "name": "Leanne Graham",
                  "username": "Bret",
                  "married": true,
                  "email": "Sincere@april.biz",
                  "address": {
                    "street": "Kulas Light",
                    "suite": "Apt. 556",
                    "city": "Gwenborough",
                    "zipcode": "92998-3874",
                    "geo": {
                      "lat": "-37.3159",
                      "lng": "81.1496"
                    }
                  },
                  "phone": "1-770-736-8031 x56442",
                  "website": "hildegard.org",
                  "company": {
                    "name": "Romaguera-Crona",
                    "catchPhrase": "Multi-layered client-server neural-net",
                    "bs": "harness real-time e-markets"
                  },
                  "languages": ["Java", "Python", "Scala", "C","C++"]
                }
                """;
        ObjectMapper mapper = new ObjectMapper();
        JsonNode roodNode = mapper.readTree(JsonSTRING);
        System.out.println("Username: " + roodNode.get("username").asText());
        System.out.println("Married: " + roodNode.get("married").asBoolean());
        JsonNode address = roodNode.get("address");
        System.out.println("Street: " + address.get("street").asText());
        System.out.println("City: " + address.get("city").asText());
        JsonNode company = roodNode.get("company");
        System.out.println("Company name: " + company.get("name").asText());

        double lat = roodNode.at("/address/geo/lat").asDouble();
        System.out.println("lat = " + lat);


    }
    @Test
    void traverseRootTest() throws Exception{
        String JsonSTRING = """
                {
                  "id": 1,
                  "name": "Leanne Graham",
                  "username": "Bret",
                  "married": true,
                  "email": "Sincere@april.biz",
                  "address": {
                    "street": "Kulas Light",
                    "suite": "Apt. 556",
                    "city": "Gwenborough",
                    "zipcode": "92998-3874",
                    "geo": {
                      "lat": "-37.3159",
                      "lng": "81.1496"
                    }
                  },
                  "phone": "1-770-736-8031 x56442",
                  "website": "hildegard.org",
                  "company": {
                    "name": "Romaguera-Crona",
                    "catchPhrase": "Multi-layered client-server neural-net",
                    "bs": "harness real-time e-markets"
                  },
                  "languages": ["Java", "Python", "Scala", "C","C++"]
                }
                """;
        ObjectMapper mapper = new ObjectMapper();
        JsonNode roodNode = mapper.readTree(JsonSTRING);
        traverse(roodNode);
    }
    void traverse(JsonNode roodNode){
        if (roodNode.isObject()){
            Iterator<String> fieldNames = roodNode.fieldNames();
            while (fieldNames.hasNext()){
                String next = fieldNames.next();
                JsonNode jsonNode = roodNode.get(next);
                traverse(jsonNode);
            }
        } else if (roodNode.isArray()) {
            for (JsonNode jsonNode : roodNode) {
                traverse(jsonNode);
            }
        }else {
            System.out.println("roodNode.asText() = " + roodNode.asText());
        }
    }
}
