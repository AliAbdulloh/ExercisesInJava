package com.company.jackson.objectmapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

public class ObjectMapperTest {
    @Test
    void simpleCodeTest() throws JsonProcessingException {
        String postJson = """
                {
                  "userId": 1,
                  "id": 1,
                  "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
                  "body": "quia et suscipit\\nsuscipit recusandae consequuntur expedita et cum\\nreprehenderit molestiae ut ut quas totam\\nnostrum rerum est autem sunt rem eveniet architecto"
                }""";
        ObjectMapper mapper = new ObjectMapper();
        Post post = mapper.readValue(postJson, Post.class);
        System.out.println(post);
        String valueAsString = mapper.writeValueAsString(post);
        System.out.println(valueAsString);
    }

    @Test
    void simpleCodeTest2() {
        System.out.println("Hello AliAbdulloh!");
    }
}
