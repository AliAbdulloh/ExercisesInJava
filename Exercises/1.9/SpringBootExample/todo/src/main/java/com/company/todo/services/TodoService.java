package com.company.todo.services;

import com.company.todo.entities.Todo;
import com.company.todo.repositories.TodoRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TodoService {
    private final TodoRepository todoRepository;

    public Optional<Todo> getTodoById(UUID id){
        return todoRepository.findById(id);
    }
    public List<Todo> getAllTodo(){
        return todoRepository.findAll();
    }
    public Todo createTodo(Todo todo){
        return todoRepository.save(todo);
    }
    public Todo updateTodo(Todo todo){
        return todoRepository.save(todo);
    }
    public void deleteTodo(UUID id){
        todoRepository.deleteById(id);
    }
}
