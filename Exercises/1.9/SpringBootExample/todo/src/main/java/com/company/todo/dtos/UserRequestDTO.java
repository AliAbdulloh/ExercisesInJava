package com.company.todo.dtos;

import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = true)
@Value
public record UserRequestDTO(String name, String email, String password, String confirmPassword) {
}
