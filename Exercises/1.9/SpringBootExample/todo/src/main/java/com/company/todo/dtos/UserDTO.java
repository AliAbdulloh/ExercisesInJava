package com.company.todo.dtos;

import com.company.todo.enums.RoleEnum;
import lombok.EqualsAndHashCode;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * DTO for {@link com.company.todo.entities.User}
 */
@EqualsAndHashCode(callSuper = true)
@Value
public record UserDTO(UUID id, String name, String email, String password, RoleEnum role, LocalDateTime createdDate,
                      LocalDateTime updatedDate) implements Serializable {
}