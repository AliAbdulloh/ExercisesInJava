package com.company.todo.dtos;

import lombok.EqualsAndHashCode;
import lombok.Value;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.UUID;

/**
 * DTO for {@link com.company.todo.entities.Todo}
 */
@EqualsAndHashCode(callSuper = true)
@Value
public record TodoDTO(UUID id, String title, String description, boolean isComplete, LocalDateTime createDate,
                      LocalDateTime updateDate) implements Serializable {
}